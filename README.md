# homie-sensors-sht30
Temp / Humidity sensor using Homie 2.0 library, locally compiled on a Wemos D1 mini

This program use Deep Sleep (interval on Homie settings), and with default settings read temperature and humidity every 300s (5min).

## Lib dependencies
I'm using PlatformIO for development, and the following dependencies are needed (you may use to install more libs if using Arduino IDE) :

    lib_deps = Homie, WEMOS SHT3x, NTPClient, Time, Timezone

I make an extensive use of the excellent Homie framework (https://github.com/marvinroger/homie-esp8266/)
